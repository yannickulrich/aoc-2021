import Data.List
-- See day24.py for why this works

toint :: String -> Integer
toint a = read a :: Integer

every n [] = []
every n as  = head as : every n (drop n as)

flatten arr = [y | x<- arr, y <- x]
find' :: (a -> Bool) -> [a] -> a
find' f (a:as)  | f a        = a
                | otherwise  = find' f as


getABCs :: Int -> String -> [Integer]
getABCs n content = map (toint.last.words) $
                    every 18 $
                    drop n $
                    lines $
                    content

getAs = getABCs 4
getBs = getABCs 5
getCs = getABCs 15

flist :: [a -> b] -> a -> [b]
flist funcs inp = map (\f -> f $ inp) $ funcs

tuplify3 :: [a] -> (a,a,a)
tuplify3 [x,y,z] = (x,y,z)

parseInp :: String -> [(Integer, Integer, Integer)]
parseInp contents = map tuplify3 $
                    transpose $
                    flist [getAs, getBs, getCs] $
                    contents


type SState = (Integer, ((Integer, Integer), (Integer, Integer)))
type State = (([(Integer, Integer)], Integer), [SState])

listOp :: State -> (Integer, Integer, Integer) -> State
listOp ((z, wj), rds) (1,b,c) = (((wj, c) : z, wj+1), rds)
listOp (((wi, cold) : z, wj), rds) (_,b,c) = ((z, wj+1), (
        wi, ((max 1 (1-d), min (9-d) 9), (wj, d))
    ) : rds)
    where
        d = cold + b



power :: [(Integer, Integer)] -> [[Integer]]
power [(a,b)] = map (\x -> [x]) $ [a..b]
power ((a,b) : as) = flatten $ map (\x -> preEach x (power as)) $ [a..b]
    where
        preEach a b = map (\x -> a:x) $ b


ds4 :: [SState] -> Integer -> (Integer, Integer)
ds4 inp n = snd $ snd $ find' (\(a,b) -> a==n) $ inp

buildNum1 :: [SState] -> (Integer, Integer) -> Integer
buildNum1 inp (i, w) = (go (i, w)) + (go $ addW $ ds4 inp i)
    where
        go (ii, ww) = 10^(13-ii) * ww
        addW (i, d) = (i, d+w)

buildNum :: [SState] -> [Integer] -> Integer
buildNum inp ws = sum $ map (buildNum1 inp) $ zip (map fst $ inp) ws

getAllNum :: [SState] -> [Integer]
getAllNum inp = map (buildNum inp) $
                power $
                map (fst.snd) $
                inp

solve :: String -> [Integer]
solve contents = getAllNum $
                 snd $
                 foldl listOp (([],0),[]) $
                 parseInp $
                 contents

solveDay24A = maximum . solve
solveDay24B = minimum . solve

main = do
    contents <- readFile "input24.txt"
    print $ solveDay24A $ contents
    print $ solveDay24B $ contents
