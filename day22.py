import re
import sys


def chopInt(iA0, iA1, iB0, iB1):
    if iA0 == iA1:
        return []
    if iB0 == iB1:
        return [(iA0, iA1, False)]
    if iB0 < iB1 <= iA0 < iA1:
        #B| |      
        #A   |          |
        #     CCCCCCCCCC
        return [(iA0, iA1, False)]
    elif iB0 < iA0 < iB1 < iA1:
        #B|       |
        #A   |          |
        #     BBBB CCCCC
        return [(iA0, iB1, True), (iB1, iA1, False)]
    elif iA0 < iB0 < iB1 < iA1:
        #B         |  |
        #A   |          |
        #     AAAAA BB C
        return [(iA0, iB0,False), (iB0, iB1,True), (iB1, iA1,False)]
    elif iA0 < iB0 < iA1 < iB1:
        #B         |       |
        #A   |          |
        #     AAAAA BBBB
        return [(iA0, iB0,False), (iB0, iA1,True)]
    elif iA0 < iA1 < iB0 < iB1:
        #B                | |      
        #A   |          |
        #     AAAAAAAAAA    
        return [(iA0, iA1, False)]
    elif iB0 <= iA0 < iA1 <= iB1:
        #B |                |      
        #A   |          |
        #     BBBBBBBBBB          
        return [(iA0, iA1, True)]
    elif iA0 < iA1 == iB0 < iB1:
        #B              |   |      
        #A   |          |
        #     AAAAAAAAAA
        return [(iA0, iA1, False)]
    elif iA0 == iB0 < iB1 < iA1:
        #B   |      |              
        #A   |          |
        #     BBBBBB CCC
        return [(iA0, iB1, True), (iB1, iA1, False)]
    elif iA0 < iB0 < iA1 == iB1:
        #B      |       |          
        #A   |          |
        #     AA BBBBBBB
        return [(iA0, iB0, False), (iB0, iA1, True)]
    else:
        print(iA0, iA1, iB0, iB1)


def splitCube(cA, cB):
    # This build c1 \ c2 by returning 26 cuboids
    (xA0,xA1), (yA0,yA1), (zA0,zA1) = cA
    (xB0,xB1), (yB0,yB1), (zB0,zB1) = cB

    if xA0 > xB1 or xA1 < xB0:
        yield cA
        return
    if yA0 > yB1 or yA1 < yB0:
        yield cA
        return
    if zA0 > zB1 or zA1 < zB0:
        yield cA
        return

    for x0,x1,mX in chopInt(xA0, xA1, xB0, xB1):
        if not mX:
            yield ((x0,x1), (yA0,yA1), (zA0, zA1))
            continue

        for y0,y1,mY in chopInt(yA0, yA1, yB0, yB1):
            if not mY:
                yield ((x0,x1), (y0,y1), (zA0, zA1))
                continue

            for z0,z1,mZ in chopInt(zA0, zA1, zB0, zB1):
                if not mZ:
                    yield ((x0,x1), (y0,y1), (z0, z1))
                    continue


def build(dat):
    on = []
    for onoff, x0, x1, y0, y1, z0, z1 in dat:
        c = (
            (x0, x1+1),
            (y0, y1+1),
            (z0, z1+1)
        )
        if onoff == 'on':
            on.append(c)
        else:
            on = [i for j in on for i in list(splitCube(j, c)) ]
    return on


def volume(on):
    vol = 0
    while len(on) > 0:
        c = on.pop()
        on = [i for j in on for i in list(splitCube(j, c)) ]
        (x0,x1), (y0,y1), (z0,z1) = c
        vol += (x1-x0) * (y1-y0) * (z1-z0)
        #sys.stderr.write("%5d\b\b\b\b\b" % len(on))
        #sys.stderr.flush()
    return vol


def play(dat):
    print(volume(build(dat)))

txt = open('input22.txt').read()
dat = [
    (onoff, int(x0), int(x1), int(y0), int(y1), int(z0), int(z1))
    for onoff, x0, x1, y0, y1, z0, z1 in re.findall(
        "([onf]*) "
        "x=([-\d]*)\.\.([-\d]*),"
        "y=([-\d]*)\.\.([-\d]*),"
        "z=([-\d]*)\.\.([-\d]*)", txt
    )
]

play([
    (onoff, x0, x1, y0, y1, z0, z1)
    for onoff, x0, x1, y0, y1, z0, z1 in dat
    if -50 <= x0 <= x1 <= 50 and
       -50 <= y0 <= y1 <= 50 and
       -50 <= z0 <= z1 <= 50
])

play(dat)
