-- This converts a single String into an Integer
toint :: String -> Int
toint a = read a :: Int

tuplify2 :: [a] -> (a,a)
tuplify2 [x,y] = (x,y)

loadLineDot :: String -> (Int, Int)
loadLineDot a = tuplify2 $ map toint $ words $ map repl $ a
    where
        repl ',' = ' '
        repl a = a

lineType :: String -> Char
lineType a  | length a == 0  = 'E'
            | length a < 10  = 'D'
            | otherwise      = 'F'

loadLineFold :: String -> (Char, Int)
loadLineFold a = (head (w!!2), toint $ w !! 3)
    where
        w = words $ map repl $ a
        repl '=' = ' '
        repl a = a



getmat :: Int -> Int -> [[(Int, Int)]]
getmat sizeX sizeY = map (zip [0..]) $
                     map (replicate sizeX) $
                     [0..sizeY-1]

mkOdd :: Int -> Int
mkOdd a  | even a  = a+1
         | odd  a  = a

dots2mat :: [(Int, Int)] -> [[Bool]]
dots2mat dots = map(map (\(x,y) -> elem (x,y) dots)) $
                getmat sizeX sizeY
    where
        sizeX = mkOdd $ (maximum $ map fst $ dots) + 1
        sizeY = mkOdd $ (maximum $ map snd $ dots) + 1


yfold :: Int -> [[Bool]] -> [[Bool]]
yfold v mat = zipWith (zipWith (||)) up down
    where
        down = fst $ sp
        up = reverse $ tail $ snd $ sp
        sp = splitAt v mat


xfold :: Int -> [[Bool]] -> [[Bool]]
xfold v mat = map xfold1 mat
    where
        xfold1 l = zipWith (||) left right
            where
                left = fst $ sp
                right = reverse $ tail $ snd $ sp
                sp = splitAt v l

fold :: [[Bool]] -> (Char, Int) -> [[Bool]]
fold mat ('x', v) = xfold v mat
fold mat ('y', v) = yfold v mat


pprint :: [[Bool]] -> String
pprint mat = concat $
             map (\x -> x ++ "\n") $
             map (map repl) $
             mat
    where
        repl True = '#'
        repl False = '.'


loadGame :: String -> ([[Bool]], [(Char, Int)])
loadGame contents = (a, b)
    where a = dots2mat $
              map loadLineDot $
              filter (\l -> 'D' == lineType l) $
              lines $
              contents
          b = map loadLineFold $
              filter (\l -> 'F' == lineType l) $
              lines $
              contents

solveDay13A :: ([[Bool]], [(Char, Int)]) -> Int
solveDay13A (mat, folds) = sum $
                           map sum $
                           map (map fromEnum) $
                           fold mat $
                           head folds


parseAlphabet1 :: [String] -> [[[Bool]]]
parseAlphabet1 [] = []
parseAlphabet1 lines = (map (map repl) $ init $ fst $ b) : parseAlphabet1 (snd $ b)
    where b = splitAt 7 lines
          repl '#' = True
          repl '.' = False

matchLetter :: Int -> [[Bool]] -> [[[Bool]]] -> Char
matchLetter c sm alphabet  | sm == head alphabet  = toEnum c
                           | otherwise  = matchLetter (c+1) sm $ tail alphabet

getBlock :: [[Bool]] -> Int -> [[Bool]]
getBlock code n = map (split1 n) $ code
    where
        split1 n c = fst $ splitAt 5 $ snd $ splitAt (5*n) $ c

decode :: [[[Bool]]] -> [[Bool]] -> String
decode alphabet code = map decode1 $ [0..(div (length $ head $ code) 5) - 1]
    where
        decode1 n = matchLetter 65 (getBlock code n) alphabet



solveDay13B :: [[[Bool]]] -> ([[Bool]], [(Char, Int)]) -> String
solveDay13B alphabet (mat, folds) = decode alphabet $
                           foldl fold mat $
                           folds


main = do
    contents <- readFile "alphabet.txt"
    let alphabet = parseAlphabet1 $ lines $ (contents++"\n")
    contents <- readFile "input13.txt"
    let ans = loadGame contents
    print $ solveDay13A $ ans
    putStrLn $ solveDay13B alphabet $ ans
