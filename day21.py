import re
from memoise import memoised


txt = open('input21.txt').read()

def roll_die1():
    global die
    global c
    c += 1
    die = (die + 1) % 100
    return die+1

def roll_die3():
    return roll_die1() + roll_die1() + roll_die1()

c = 0
die = -1
score1 = 0
score2 = 0

p1,p2 = [
    int(i)-1
    for i in re.findall("Player \d starting position: (\d*)", txt)
]

while True:
    p1 = (p1 + roll_die3()) % 10
    score1 += (p1+1)
    if score1 >= 1000:
        break

    p2 = (p2 + roll_die3()) % 10
    score2 += (p2+1)
    if score2 >= 1000:
        break

print(min(score1,score2) * c)


score1 = 0
score2 = 0

p1,p2 = [
    int(i)-1
    for i in re.findall("Player \d starting position: (\d*)", txt)
]


dieV = {
    3: 1,
    4: 3,
    5: 6,
    6: 7,
    7: 6,
    8: 3,
    9: 1
}


@memoised
def play(p1, p2, s1, s2):
    if s2 >= 21: return 0, 1
    w1 = 0
    w2 = 0
    for v, n in dieV.items():
        p = (p1+v) % 10
        v2, v1 = play(p2, p, s2, s1 + (p+1))
        w1 = w1 + v1 * n
        w2 = w2 + v2 * n
    return w1,w2
print(max(play(p1,p2,0,0)))
