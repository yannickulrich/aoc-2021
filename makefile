GHC=ghc
TIME=/usr/bin/time -f "\n Took %Us\n"

targets=day01 \
        day02 \
        day03 \
        day04 \
        day05 \
        day06 \
        day07 \
        day08 \
        day09 \
        day10 \
        day11 \
        day12 \
        day13 \
        day14 \
        day16 \
        day17 \
        day18 \
        day19 \
        day20 \
        day21 \
        day22 \
        day24 \
        day25


all: $(targets)
check: $(addprefix test-,$(targets)) \
       $(addprefix test-py-,$(targets)) \
       test-py-day15

day%: day%.hs
	$(GHC) -o $@ $<

input%.txt:
	./tools/aoc.sh inp $@ > $@

test-day23: day23.txt
	echo `cat day23.txt | perl -ne 'print if s/.* (\+ +[\d\*]+) .*/\1/g' | head -n12` | cut -c2- | bc
	echo `cat day23.txt | perl -ne 'print if s/.* (\+ +[\d\*]+) .*/\1/g' | tail -n25` | cut -c2- | bc

test-day%: day% input%.txt
	test "$$(./tools/aoc.sh ans $< ' ')" = "$$($(TIME) ./$< | tr '\n' ' ')"

test-py-day%: day%.py input%.txt
	test "$$(./tools/aoc.sh ans $< ' ')" = "$$($(TIME) python3 $< | tr '\n' ' ')"

clean:
	rm -f $(targets) *.o *.hi input*
