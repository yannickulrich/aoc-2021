hexmap = {
    '0': '0000',
    '1': '0001',
    '2': '0010',
    '3': '0011',
    '4': '0100',
    '5': '0101',
    '6': '0110',
    '7': '0111',
    '8': '1000',
    '9': '1001',
    'A': '1010',
    'B': '1011',
    'C': '1100',
    'D': '1101',
    'E': '1110',
    'F': '1111'
}

txt = 'D2FE28'


def parse_lit(bb):
    ans = ""
    while True:
        ans += bb[1:5]
        if bb[0] == '0':
            break
        bb = bb[5:]
    return int(ans,2), bb[5:]


def parse_packet(b):
    if len(b) < 7:
        return [], "", 0
    vers = int(b[0:3],2)
    typ = int(b[3:6],2)

    if typ == 4:
        v, rem = parse_lit(b[6:])
        return v, rem, vers
    else:
        vacc = vers
        ans = []
        ltID = b[6]
        if ltID == '0':
            length = int(b[7:22],2)
            rem = b[22:22+length]
            while len(rem) > 0:
                an, rem, vv = parse_packet(rem)
                vacc += vv
                ans.append(an)
            rem = b[22+length:]

        elif ltID == '1':
            nsub = int(b[7:18],2)
            rem = b[18:]
            for i in range(nsub):
                an, rem, vv = parse_packet(rem)
                vacc += vv
                ans.append(an)

        if typ == 0:
            return (sum(ans), rem, vacc)
        elif typ == 1:
            acc = 1
            for i in ans:
                acc *= i
            return (acc, rem, vacc)
        elif typ == 2:
            return (min(ans), rem, vacc)
        elif typ == 3:
            return (max(ans), rem, vacc)
        elif typ == 5:
            a,b = ans
            if a > b:
                return (1,rem, vacc)
            else:
                return (0,rem, vacc)
        elif typ == 6:
            a,b = ans
            if a < b:
                return (1,rem, vacc)
            else:
                return (0,rem, vacc)
        elif typ == 7:
            a,b = ans
            if a == b:
                return (1,rem, vacc)
            else:
                return (0,rem, vacc)
        return (ans, rem, vacc)

def parse_hex(h):
    return parse_packet(''.join(hexmap[i] for i in h))

txt = open('input16.txt').read().strip()

ans, _, versionAcc = parse_hex(txt)
print(versionAcc)
print(ans)
