import re
import string
import copy

txt = open('input12.txt').read()

def isin(a, c):
    if a == c[0]:
        return c[1]
    elif a == c[1]:
        return c[0]
    else:
        return False


map = re.findall("(.*)-(.*)", txt)
all_caves = set(j for i in map for j in i)
connections = {
    i: list(filter(
        lambda x: type(x) == str,
        [isin(i, j) for j in map]
    ))
    for i in all_caves
}
visited = {
    i: 0 for i in all_caves
}

def cango(c, visited):
    if c == 'start':
        return False
    if c[0] in string.ascii_lowercase:
        if visited[c] == 0:
            return True
        else:
            return False
    return True


def flatten(lst):
    ans = []
    for i in lst:
        if i == []:
            continue
        if type(i[0]) == str:
            ans.append(i)
        else:
            for j in i:
                ans += flatten(lst)
    return ans


def search(c, path, visited):
    visited = {
        i: (
            v + 1 if i == c else v
        )
        for i,v in visited.items()
    }
    path = path[:] + [c]

    if c == 'end':
        return [path]

    opts = [
        i
        for i in connections[c]
        if cango(i, visited)
    ]

    ans = []
    for i in opts:
        ans += search(i, path, visited)
    return ans


print(len(search('start', [], visited)))


def cango(c, visited):
    small = [
        i
        for i in visited.keys()
        if i[0] in string.ascii_lowercase and \
           i!='start' and\
           i!='end'
    ]
    if c == 'start':
        return False
    if c in small:
        n = max(visited[i] for i in small)
        if n == 2:
            if visited[c] == 0:
                return True
            else:
                return False
        else:
            return True
    return True

print(len(search('start', [], visited)))
