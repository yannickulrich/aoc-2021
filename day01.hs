smallerQ :: (Integer, Integer) -> Integer
smallerQ (a, b) | a >= b = 0
                | a < b = 1

-- This converts a single String into an Integer
toint :: String -> Integer
toint a = read a :: Integer

-- This takes a list [a,b,c,d] and returns
-- [(a,b), (b,c), (c,d)]
doublezip lst = zip (init lst) (tail lst)

-- To solve Day 1 we need to take the list we get from file and
-- convert to int. Next, we double-zip to get consecutive elements
-- together. Then we just need to compare using smallerQ and sum the
-- 1s 

solveDay1A ::  [String] -> Integer
solveDay1A depths = sum $
                    map smallerQ $
                    doublezip $
                    map toint depths

-- For the bonus problem we need something that can do moving windows.
-- This is taken from https://stackoverflow.com/a/46618036
groupBy :: Int -> [a] -> [[a]]
groupBy n xs = drop n $ go [] xs
    where
        go _ [] = []
        go l (x:xs) = (x:t) : go (x:l) xs
            where t = take n l

windowSum :: Int -> [Integer] -> [Integer]
windowSum n lst = map sum $ groupBy n lst

solveDay1B :: [String] -> Integer
solveDay1B depths = sum $
                    map smallerQ $
                    doublezip $
                    windowSum 2 $
                    map toint depths


main = do
    contents <- readFile "input01.txt"
    print $ solveDay1A $ lines $ contents
    print $ solveDay1B $ lines $ contents
