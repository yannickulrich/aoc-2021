import Data.Bits

mask2int :: [Bool] -> Int
mask2int bm = sum $
              map (\(x,y) -> x*(shiftL 1 (y-1))) $
              zip (map fromEnum $ bm)
                  (reverse $ [0..(length bm)])

-- This converts a single String into an Integer
tointBin :: String -> Int
tointBin a = mask2int $ map (\x -> x=='1') $ a

-- This returns an array of the same length with true if the bit at
-- that shift (0 is LSB) is 1
bitAt :: Int -> [Int] -> [Bool]
bitAt n lst = map (\x -> x .&. t /= 0) $
              lst
    where t = shiftL 1 n

nbits :: Int -> Int
nbits 0 = 0
nbits n = 1 + nbits (shiftR n 1)

-- This takes a shift and gets the most common bit at that shift (0 
-- is the LSB)
mostCommon :: [Int] -> Int -> Bool
mostCommon lst n = 2*t >= (length lst)
    where t = sum $
              map fromEnum $
              bitAt n lst

getRange :: [Int] -> [Int]
getRange lst = [0..((nbits $ maximum lst)-1)]


-- This returns the gamma value, i.e. it looks for the most common
-- bits in lst and re-assembles them
gamma :: [Int] -> Int
gamma lst = mask2int $
            map (mostCommon lst) $
            reverse $
            getRange lst
eps :: [Int] -> Int
eps lst = mask2int $
          map not $
          map (mostCommon lst) $
          reverse $
          getRange lst

solveDay3A :: [String] -> Int
solveDay3A log = (eps t) * (gamma t)
    where t = map tointBin $ log

ox :: [Int] -> Int
ox lst = go (reverse $ getRange lst) lst
    where
        go _ [a] = a
        go (n:ns) lst = go (ns) $
                        filter (\x -> (x .&. (shiftL 1 n) /= 0) == t ) $
                        lst
            where t = mostCommon lst n

co2 :: [Int] -> Int
co2 lst = go (reverse $ getRange lst) lst
    where
        go _ [a] = a
        go (n:ns) lst = go (ns) $
                        filter (\x -> (x .&. (shiftL 1 n) /= 0) == t ) $
                        lst
            where t = not $ mostCommon lst n


solveDay3B :: [String] -> Int
solveDay3B log = (ox t) * (co2 t)
    where t = map tointBin $ log

main = do
    contents <- readFile "input03.txt"
    print $ solveDay3A $ lines $ contents
    print $ solveDay3B $ lines $ contents
