import Data.List

-- This converts a single String into an Integer
toint :: Char -> Int
toint c = fromEnum c - 48


nL :: Int -> (Int, Int) -> [(Int, Int)]
nL l (x,y) | x > 0     = [(x-1,y)]
           | otherwise = []
nR :: Int -> (Int, Int) -> [(Int, Int)]
nR l (x,y) | x < l-1   = [(x+1,y)]
           | otherwise = []
nT :: Int -> (Int, Int) -> [(Int, Int)]
nT l (x,y) | y > 0     = [(x,y-1)]
           | otherwise = []
nB :: Int -> (Int, Int) -> [(Int, Int)]
nB l (x,y) | y < l-1   = [(x,y+1)]
           | otherwise = []

nAll :: (Int, Int) -> Int -> [(Int, Int)]
nAll (x,y) l = [(x,y)] ++
               (nL l (x,y)) ++ (nR l (x,y)) ++
               (nT l (x,y)) ++ (nB l (x,y))

get_neighbours :: [[Int]] -> (Int,Int) -> [(Int, Int, Int)]
get_neighbours dat (x,y) = map (\(x,y) -> (x,y, dat !! y !! x)) $
                           nAll (x,y) $
                           length $
                           dat

indexlist :: [[Int]] -> [[(Int,Int)]]
indexlist dat = map (\x -> zip [0..] $ replicate (length dat) x) $
                [0..(length dat-1)]

flatten arr = [y | x<- arr, y <- x]

third :: (Int, Int, Int) -> Int
third (x,y,h) = h
honly = map third

lowp :: [(Int,Int,Int)] -> [(Int,Int,Int)] -> [(Int,Int,Int)]
lowp points state  | h < min   = (head$points) : state
                   | otherwise = state
    where min = minimum $ honly $ tail $ points
          h = third $ head $ points

dat2lowp :: [[Int]] -> [(Int,Int,Int)]
dat2lowp dat = foldr lowp [] $
               map (get_neighbours dat) $
               flatten $
               indexlist $
               dat

solveDay9A :: [String] -> Int
solveDay9A lines = sum $
                   map (\x->x+1) $
                   map third $
                   dat2lowp $
                   map (map toint) lines

ascendableNeighbours :: [[Int]] -> (Int,Int,Int) -> [(Int,Int,Int)]
ascendableNeighbours dat (x,y,h) = filter (\(xx,yy,hh) -> hh>h) $
                                   filter (\(xx,yy,hh) -> hh<9) $
                                   tail $
                                   get_neighbours dat (x,y)

ascend :: [[Int]] -> (Int,Int,Int) -> [(Int,Int,Int)] -> [(Int,Int,Int)]
ascend dat point state = point : new ++ state
    where new = flatten $
                map (\p->ascend dat p state) $
                ascendableNeighbours dat point

ascendFullCount :: [[Int]] -> (Int,Int,Int) -> Int
ascendFullCount dat lp = length $
                         group $
                         sort $
                         ascend dat lp []

sizes :: [[Int]] -> [Int]
sizes dat = reverse $
            sort $
            map (ascendFullCount dat) $
            dat2lowp $
            dat

firstthree :: [Int] -> [Int]
firstthree l = [l!!0, l!!1, l!!2]

solveDay9B :: [String] -> Int
solveDay9B lines = product $
                   firstthree $
                   sizes $
                   map (map toint) lines

main = do
    contents <- readFile "input09.txt"
    print $ solveDay9A $ lines $ contents
    print $ solveDay9B $ lines $ contents
