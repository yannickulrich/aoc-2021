txt = [int(i) for i in open('input06.txt').read().strip().split(',')]

def build_state(txt):
    state = (5+max(txt)) * [0]
    for i in txt:
        state[8-i] += 1
    return state


def evolve(state):
    return [sum(state[8::7])] + state

state = build_state(txt)
for i in range(80):
    state = evolve(state)
print(sum(state))


state = build_state(txt)
for i in range(256):
    state = evolve(state)
print(sum(state))
