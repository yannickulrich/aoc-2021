import Data.Bits

mask2int :: [Bool] -> Int
mask2int bm = sum $
              map (\(x,y) -> x*(shiftL 1 (y-1))) $
              zip (map fromEnum $ bm)
                  (reverse $ [0..(length bm)])

neighbours :: Int -> Int -> [(Int, Int)]
neighbours x y = [(xx, yy) | yy <- [y-1,y,y+1],
                             xx <- [x-1,x,x+1]]


indAt :: [[Bool]] -> (Int, Int) -> Bool -> Int
indAt img (x,y) bg = mask2int $
                     map get $
                     neighbours x y
    where
        get :: (Int, Int) -> Bool
        get (xx, yy) = if (0 <= xx) && (xx <= h-1) && (0 <= yy) && (yy <= h-1)
                        then (img!!yy)!!xx
                        else bg
        h = length $ img

indexMap :: Int -> Int -> [[(Int, Int)]]
indexMap w h = map (\x -> zip [-1..w+1] $ replicate (w+2) x) $ [-1..h]

apply :: [[Bool]] -> [Bool] -> Bool -> [[Bool]]
apply img alg bg = map (map f) $ indexMap w h
    where
        f :: (Int, Int) -> Bool
        f pos = alg !! (indAt img pos bg)
        w = length $ head $ img
        h = length $        img

repl '#' = True
repl '.' = False

parseAlg :: String -> [Bool]
parseAlg contents = map repl $ head $ lines $ contents
parseImg :: String -> [[Bool]]
parseImg contents = map (map repl) $ tail $ tail $ lines $ contents

applyN :: Int -> [Bool] -> [[Bool]] -> [[Bool]]
applyN 0 alg img = img
applyN n alg img = applyN (n-1) alg $
                   apply img alg (1== (mod n 2))

flatten arr = [y | x<- arr, y <- x]

value :: [[Bool]] -> Int
value img = sum $ map fromEnum $ flatten $ img

solveDay20A :: String -> Int
solveDay20A contents = value $ applyN 2 alg img
    where
        alg = parseAlg $ contents
        img = parseImg $ contents
solveDay20B :: String -> Int
solveDay20B contents = value $ applyN 50 alg img
    where
        alg = parseAlg $ contents
        img = parseImg $ contents

main = do
    contents <- readFile "input20.txt"
    print $ solveDay20A $ contents
    print $ solveDay20B $ contents
