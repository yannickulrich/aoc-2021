import Data.Maybe

-- This converts a single String into an Integer
toint :: String -> Integer
toint a = read a :: Integer

type Inp = (Integer,Integer,Integer,Integer)
parse :: String -> Inp
parse a = (toint $ lst !! 3, toint $ lst !! 4, toint $ lst !! 6, toint $ lst !! 7)
    where
        lst = words $ map repl $ a
        repl '=' = ' '
        repl '.' = ' '
        repl ',' = ' '
        repl '\n' = ' '
        repl a = a

type State = (Integer, Integer, Integer, Integer)
step1 :: State -> State
step1 (x,y,0 ,vy) = (x   , y+vy, 0   ,vy-1)
step1 (x,y,vx,vy) = (x+vx, y+vy, vx-1,vy-1)

check :: Inp -> State -> Integer
check (txa,txb,tya,tyb) (x,y,_,_)  | y < tya                   = 0 -- stop
                                   | x > txb                   = 0
                                   | (txa <= x) && (x <= txb) &&
                                     (tya <= y) && (y <= tyb)  = 1 -- won
                                   | otherwise                 = 2 -- keep going

mymax :: Integer -> Maybe Integer -> Maybe Integer
mymax a Nothing        = Nothing
mymax a b  | Just a>b  = Just a
           | Just a<=b = b

evolve1 :: Inp -> State -> Maybe Integer
evolve1 target state = case (check target new) of
                        0 -> Nothing
                        1 -> Just $ mysnd $ new
                        2 -> mymax (mysnd new) $
                             evolve1 target new
    where
        mysnd (a,b,c,d) = b
        new = step1 $ state


getmap :: [State]
getmap = [(0,0,vx,vy) | vx <- [0..300],
                        vy <- [-100..200] ]


solveDay17A :: String -> Integer
solveDay17A contents = maximum $
                       mapMaybe (evolve1 target) $
                       getmap
    where target = parse $ contents

solveDay17B :: String -> Int
solveDay17B contents = length $
                       mapMaybe (evolve1 target) $
                       getmap
    where target = parse $ contents


main = do
    contents <- readFile "input17.txt"
    print $ solveDay17A $ contents
    print $ solveDay17B $ contents
