-- This converts a single String into an Integer
toint a = read a :: Integer

-- This takes a string of the format "1,2,3,4,5" and returns an array
-- of numbers [1,2,3,4,5]
list2int :: String -> [Integer]
list2int s = map toint $
             words $
             map repl s
    where
        repl ',' = ' '
        repl c = c

-- This returns every 7th element, i.e. [0,7,14,...]
every7 :: [Integer] -> [Integer]
every7 [] = []
every7 xs = head xs : every7 (drop 7 xs)

-- The state in this code is a list of fishes with a given age, i.e.
-- [1,2,0,0,0,10] refers to one new born fish, two one day old fishes
-- and ten five day old fishes. The state is evolved by counting the
-- number of reproduction-ready fishes in each generation (those at
-- indices 8::8) and prepending their babies to the state

-- This calculate the number of babies to be added in the current
-- generation
babies :: [Integer] -> Integer
babies state = sum $ every7 $ drop 8 $ state

-- This evolves the state by one generation
evolve :: [Integer] -> [Integer]
evolve state = babies state : state

-- This apply evolve n times.
evolveN :: Integer -> [Integer] -> [Integer]
evolveN 0 a = a
evolveN n a = evolveN (n-1) $ evolve a

-- This counts the number of times a appears in as. This could be done
-- with filter but I like the pattern thing.
countN :: [Integer] -> Integer -> Integer
countN as a = fst $ go (0,as)
    where
        go (n,[]) = (n,[])
        go (n,as') | (head $ as') == a  = go (n+1, tail as')
                   | otherwise          = go (n  , tail as')

-- This assembles the state as described above from the current age of
-- the fishes. It counts the number of fishes of a given age and
-- reverses the list because AoC gives inverse age (time till next
-- reproduction instead of clock age). It also adds 2 to the age
-- because it is assumed all initial fishes are ready to go after at
-- most six days meaning they are at least two days old.
buildstate :: [Integer] -> [Integer]
buildstate txt = reverse $
                 map (countN txt) $
                 [0..8]

solveDay6A :: String -> Integer
solveDay6A s = sum $
               evolveN 80 $
               buildstate $
               list2int $
               s

solveDay6B :: String -> Integer
solveDay6B s = sum $
               evolveN 256 $
               buildstate $
               list2int $
               s

main = do
    contents <- readFile "input06.txt"
    print $ solveDay6A $ contents
    print $ solveDay6B $ contents
