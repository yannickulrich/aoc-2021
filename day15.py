txt = open('input15.txt').read()
dat = [[int(i) for i in j] for j in txt.splitlines()]



def dijkstra(dat, start, finish):
    n = len(dat)
    def pos2ind(p):
        x,y = p
        return y*n+x

    def ind2pos(i):
        return (i%n,i//n)

    def neighbours(i):
        x,y = ind2pos(i)
        if x > 0:
            yield pos2ind((x-1,y))
        if x < n-1:
            yield pos2ind((x+1,y))
        if y > 0:
            yield pos2ind((x,y-1))
        if y < n-1:
            yield pos2ind((x,y+1))

    def datat(i):
        x,y = ind2pos(i)
        return dat[y][x]

    inf = 10000000
    dist = {i: inf for i in range(n*n)}
    prev = {i: None for i in range(n*n)}
    Q = [i for i in range(n*n)]

    dist[pos2ind(start)] = 0

    while len(Q) > 0:
        u = min(Q, key=dist.get)
        Q.remove(u)

        if u == pos2ind(finish):
            break

        for v in neighbours(u):
            if v not in Q:
                continue
            alt = dist[u] + datat(v)
            if alt < dist[v]:
                dist[v] = alt
                prev[v] = u

    acc = 0
    u = pos2ind(finish)
    S = [u]
    while True:
        u = prev[u]
        if u == None:
            break
        S.append(u)
        acc += datat(u)

    return sum([datat(i) for i in S[:-1]])



def astar(dat, start, finish):

    n = len(dat)
    def pos2ind(p):
        x,y = p
        return y*n+x

    def ind2pos(i):
        return (i%n,i//n)

    def neighbours(i):
        x,y = ind2pos(i)
        if x > 0:
            yield pos2ind((x-1,y))
        if x < n-1:
            yield pos2ind((x+1,y))
        if y > 0:
            yield pos2ind((x,y-1))
        if y < n-1:
            yield pos2ind((x,y+1))

    def h(i):
        x,y = ind2pos(i)
        xf,yf = finish
        return (abs(x - xf) + abs(y - yf))

    def datat(i):
        x,y = ind2pos(i)
        return dat[y][x]

    inf = 10000000
    prev = {i: None for i in range(n*n)}
    Q = [pos2ind(start)]
    #Q = [i for i in range(n*n)]

    gdist = {i: inf for i in range(n*n)}
    gdist[pos2ind(start)] = 0

    fdist = {i: inf for i in range(n*n)}
    fdist[pos2ind(start)] = h(pos2ind(start))

    while len(Q) > 0:
        u = min(Q, key=fdist.get)
        Q.remove(u)

        if u == pos2ind(finish):
            break

        for v in neighbours(u):
            tgs = gdist[u] + datat(v)
            if tgs < gdist[v]:
                gdist[v] = tgs
                fdist[v] = tgs + h(v)
                prev[v] = u

                if v not in Q:
                    Q.append(v)


    acc = 0
    u = pos2ind(finish)
    S = [u]
    while True:
        u = prev[u]
        if u == None:
            break
        S.append(u)
        acc += datat(u)

    return sum([datat(i) for i in S[:-1]])

print(astar(dat, (0,0), (len(dat)-1, len(dat)-1)))


def datnew(dat):
    def wrap(i, n):
        if n == 0: return i
        if i+1 > 9:
            return wrap(1, n-1)
        else:
            return wrap(i+1, n-1)

    for r in range(len(dat)*5):
        rr = r % len(dat)
        dy = r // len(dat)
        acc = []
        for dx in range(5):
            acc += [wrap(j, dx + dy) for j in dat[rr]]

        yield(acc)


dn = list(datnew(dat))
print(astar(dn, (0,0), (len(dn)-1, len(dn)-1)))
