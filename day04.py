import copy
import re

txt = open('input04.txt').read().strip()

def board2mat(b):
    return [[int(i) for i in re.split(' +', j.strip())] for j in b.split('\n')]

dat = txt.split("\n\n")
nmbr = [int(i) for i in dat[0].split(',')]
boards = [board2mat(i) for i in dat[1:]]


mask = [
    [
        [False for i in range(5)]
        for j in range(5)
    ]
    for k in range(len(boards))
]

def drawN_board(n, state):
    m, b = state
    m = copy.deepcopy(m)
    try:
        ind = [i for j in b for i in j].index(n)

        m[ind//5][ind%5] = True
        return m, b
    except:
        return state


def drawN(n, states):
    return [
        drawN_board(n, i) for i in states
    ]


def transpose(m):
    return list(zip(*m))


def checkwin_board(state):
    m, _ = state
    if any(all(i) for i in m):
        return True
    if any(all(i) for i in transpose(m)):
        return True
    return False



def checkwin(state):
    winmask = [checkwin_board(i) for i in state]
    if any(winmask):
        # This assume at most one person wins
        ind = winmask.index(True)
        return ind
    else:
        return -1


def calc_score(state, last):
    m,b = state
    return last * sum(
        b
        for a, b in zip(
            [i for j in m for i in j],
            [i for j in b for i in j]
        )
        if not a
    )

state = list(zip(mask, boards))
for i in nmbr:
    state = drawN(i, state)
    won = checkwin(state)
    if won >= 0:
        print(calc_score(state[won], i))
        break

state = list(zip(mask, boards))
for i in nmbr:
    state = drawN(i, state)
    stateNew = list(
        filter(lambda x : not checkwin_board(x), state)
    )
    if len(stateNew) == 0:
        winning_state = [j for j in state if checkwin_board(j)]
        print(calc_score(winning_state[0], i))
        break
    state = stateNew
