def pprint(img):
    print(
        '\n'.join(
            ''.join(
                '#' if i else '.'
                for i in j
            )
            for j in img
        )
    )
def get_neighbours(x,y):
    spanX = [x-1,x,x+1]
    spanY = [y-1,y,y+1]
    return [(x,y) for y in spanY for x in spanX]

def mask2int(arr):
    i = 0
    for b in arr:
        i = (i << 1) | b
    return i


def indAt(img, x, y,bg):
    w = len(img[0])
    h = len(img)
    def get(xx,yy):
        if 0 <= xx <= w-1 and 0 <= yy <= h-1:
            return img[yy][xx]
        else:
            return bg
    return mask2int([
        get(xx,yy)
        for (xx,yy) in get_neighbours(x,y)
    ])


def apply(img, alg,bg):
    new = []
    for y in range(-1, len(img)+1):
        line = []
        for x in range(-1, len(img[0])+1):
            line.append(alg[indAt(img, x, y,bg)])
        new.append(line)
    return new

txt = open('input20.txt').read()

alg=[1 if j=='#' else 0 for j in txt.splitlines()[0]]
img = [
    [1 if j=='#' else 0 for j in i]
    for i in txt.splitlines()[2:]
]

ans = apply(apply(img, alg,0), alg,1)
print(sum(sum(i) for i in ans))


for j in range(50):
    img = apply(img, alg, j%2)

print(sum(sum(i) for i in img))
