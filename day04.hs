-- This converts a single String into an Integer
toint :: String -> Integer
toint a = read a :: Integer

-- This loads the first line of the file, replaces the comma with ' ',
-- splits into words (hence the repl) and maps toint
loadNumbers :: String -> [Integer]
loadNumbers a = map toint $
                words $
                map repl $
                head $
                lines a
    where
        repl ',' = ' '
        repl c = c

-- This takes the nth board (drop the first 2+6*n lines, take the
-- first five), maps words (which is fine with multiple spaces)
extractNthBoard :: String -> Int -> [[Integer]]
extractNthBoard txt n = map (map toint) $
                        map  words $
                        take 5 $
                        drop (2+6*n) $
                        lines txt

emptyMask :: [[Bool]]
emptyMask = [[False,False,False,False,False],
             [False,False,False,False,False],
             [False,False,False,False,False],
             [False,False,False,False,False],
             [False,False,False,False,False]]

-- This returns a list of board state (mask, board) where mask is a
-- boolean matrix that's true if that number has been called
type State = ([[Bool]], [[Integer]])

buildState :: [[[Integer]]] -> [State]
buildState boards = zip
                        (replicate (length boards) emptyMask)
                        boards

flatten arr = [y | x<- arr, y <- x]
allIndices arr n = filter fst $
                   zip (map (\x -> x==n) $ arr) [0..length arr]
index arr n = case(length t) of
                0 -> -1
                (_) -> snd $ head $ t
    where t = allIndices arr n

changeAt (-1) _ arr = arr
changeAt n what arr = (take n arr) ++ [what] ++ (drop (n+1) arr)

-- Recursive transpose, from https://stackoverflow.com/q/2578930
transpose:: [[a]]->[[a]]
transpose ([]:_) = []
transpose x = (map head x) : transpose (map tail x)

-- Evolve a single board state
evolve1 :: Integer -> State -> State
evolve1 n (mask, board) = (
        changeAt y (changeAt x True $ mask!!y) mask,
        board
    )
    where
        y = t `div` 5
        x = t `mod` 5
        t = index (flatten board) n

evolveN :: Integer -> [State] -> [State]
evolveN n states = map (evolve1 n) $ states

-- This checks if a given board has one
check1 :: State -> Bool
check1 (mask, board) = (or $ map and $ mask) ||
                       (or $ map and $ transpose $ mask)
-- This takes a list of states and a list of numbers to be called. It
-- then calls the first, evolves all states and returns the new list
-- of states and the as-of-yet uncalled numbers
callNumber :: ([State], [Integer]) -> ([State], [Integer])
callNumber (states, (n:ns)) = (evolveN n states, (ns))

-- This checks if any of the boards have won
wonQ :: [State] -> Bool
wonQ states = or $ map check1 $ states

-- This plays the game until someone wins. It recursively calls
-- itself, announces a number and check if someone has won. If so, it
-- returns the boards that won and the number that made the call
playGameA :: ([State], [Integer]) -> ([State], Integer)
playGameA (states, n) = case (wonQ $ fst t) of
    True -> (filter check1 $ fst t, head $ n)
    False -> playGameA t
    where
        t = callNumber (states, n)

-- This scores a single board by finding the unmarked fields with zip.
score1 :: (State, Integer) -> Integer
score1 ((mask, board), b) = (sum unmarked) * b
    where unmarked = map snd $
                     filter fst $
                     zip
                        (map not $ flatten $ mask)
                        (flatten $ board)

-- This returns a list of all scores, should there be multiple
scores :: ([State], Integer) -> [Integer]
scores (states, b) = map (\x -> score1 (x,b)) $ states

solveDay4A :: String -> Integer
solveDay4A contents = head $ scores $ playGameA (
        buildState $ map (extractNthBoard contents) [0..99],
        loadNumbers $ contents
    )


-- This plays until everyone has won. It also recursively calls itself,
-- announces a number and the eliminates every winner. If the next
-- call would result in an empty list, i.e. the last winner, it
-- returns this player and the number that would let them win
playGameB :: ([State], [Integer]) -> ([State], Integer)
playGameB (states, n) = case (length $ fst tnew) of
    0 -> (fst t, head $ n)
    _ -> playGameB tnew
    where
        tnew = (filter (not.check1) $ fst t, snd $ t)
        t = callNumber (states, n)


solveDay4B :: String -> Integer
solveDay4B contents = head $ scores $ playGameB (
        buildState $ map (extractNthBoard contents) [0..99],
        loadNumbers $ contents
    )

main = do
    contents <- readFile "input04.txt"
    print $ solveDay4A $ contents
    print $ solveDay4B $ contents
