import re

txt = open('input05.txt').read()

dat = [
    (int(x1), int(y1), int(x2), int(y2))
    for x1,y1,x2,y2 in re.findall(
        "(\d*),(\d*) -> (\d*),(\d*)",
        txt
    )
]

mat = [
    [0 for i in range(1000)]
    for j in range(1000)
]

def get_range(a,b):
    if a < b:
        return range(a,b+1)
    else:
        return reversed(range(b,a+1))


for x1,y1,x2,y2 in dat:
    if x1 == x2:
        for i in get_range(y1,y2):
            mat[i][x1] += 1
    elif y1 == y2:
        for i in get_range(x1,x2):
            mat[y1][i] += 1

print(
    len([i for j in mat for i in j if i >= 2])
)



mat = [
    [0 for i in range(1000)]
    for j in range(1000)
]

for x1,y1,x2,y2 in dat:
    if x1 == x2:
        for i in get_range(y1,y2):
            mat[i][x1] += 1
    elif y1 == y2:
        for i in get_range(x1,x2):
            mat[y1][i] += 1
    else:
        # Assume 45deg
        for x,y in zip(get_range(x1,x2), get_range(y1,y2)):
            mat[y][x] += 1


print(
    len([i for j in mat for i in j if i >= 2])
)
