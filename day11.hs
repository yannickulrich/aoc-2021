import Data.List
-- This converts a single String into an Integer
toint :: Char -> Int
toint c = fromEnum c - 48

spanX :: Int -> [Int]
spanX 0 = [0, 1]
spanX 9 = [9, 8]
spanX x = [x, x-1, x+1]
spanY :: Int -> [Int]
spanY 0 = [0, 1]
spanY 9 = [9, 8]
spanY y = [y, y-1, y+1]

neighbours :: (Int,Int) -> [(Int,Int)]
neighbours (x,y) = [ (xx,yy) | xx<-spanX x, yy<-spanY y ]

flatten arr = [y | x<- arr, y <- x]

type FullP = (Int,Int,Int, Bool)
iv2xyv :: (Int, Int) -> FullP
iv2xyv (i,v) = (mod i 10, div i 10, v, False)

inc :: [FullP] -> [FullP]
inc mat = map (\(x,y,v,_)->(x,y,v+1,False)) $ mat

substepFi :: FullP->([FullP],[(Int,Int)])->([FullP],[(Int,Int)])
substepFi (x,y,v,True ) (m, n)           = ((x,y,v,True ):m, n)
substepFi (x,y,v,False) (m, n)  | v <=9  = ((x,y,v,False):m, n)
                                | v > 9  = ((x,y,v,True ):m, n
                                    ++ neighbours (x,y))

-- This counts the number of times a appears in as. This could be done
-- with filter but I like the pattern thing.
countN :: [(Int,Int)] -> (Int,Int) -> Int
countN as a = fst $ go (0,as)
    where
        go (n,[]) = (n,[])
        go (n,as') | (head $ as') == a  = go (n+1, tail as')
                   | otherwise          = go (n  , tail as')


substepInc :: [(Int,Int)] -> FullP -> FullP
substepInc lst (x,y,v,b) = (x,y,v + (countN lst (x,y)),b)

substep :: ([FullP],Bool) -> ([FullP],Bool)
substep (mat,_) = (map (substepInc n) $ fst $ ans,length n == 0)
    where n = sort $ snd $ ans
          ans = foldr substepFi ([],[]) $ mat

cut :: FullP -> FullP
cut (x,y,v,b)  | v <=9  = (x,y,v,b)
               | v > 9  = (x,y,0,b)

step :: [FullP] -> [FullP]
step mat = map cut $
           fst $
           until snd substep $
           (inc $ mat, False)

flash :: FullP -> Bool
flash (_,_,_,f) = f

solveDay11A :: [String] -> Int
solveDay11A lines = sum $
                    map fromEnum $
                    map flash $
                    flatten $
                    take 101 $
                    iterate step $
                    map iv2xyv $
                    zip [0..] $
                    map toint $
                    flatten $
                    lines



syncQ :: [FullP] -> Bool
syncQ mat = and $ map flash $ mat

findFirst :: Int -> [Bool] -> Int
findFirst n as  | head as   = n
                | otherwise = findFirst (n+1) $
                              tail as


solveDay11B :: [String] -> Int
solveDay11B lines = findFirst 0 $
                    map syncQ $
                    iterate step $
                    map iv2xyv $
                    zip [0..] $
                    map toint $
                    flatten $
                    lines


main = do
    contents <- readFile "input11.txt"
    print $ solveDay11A $ lines $ contents
    print $ solveDay11B $ lines $ contents
