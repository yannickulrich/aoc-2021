import Data.List
-- This converts a single String into an Integer
toint a = read a :: Integer

-- This loads a single line of the format x1,y1 -> x2,y2 by replacing
-- the arrow and the comma with spaces, splitting into words (hence the
-- repl) and mapping toint
type Instr = ((Int, Int), (Int, Int))
loadLine :: String -> Instr
loadLine l = tuplify $
             map (fromIntegral.toint) $
             words $
             map repl l
    where
        repl ',' = ' '
        repl '-' = ' '
        repl '>' = ' '
        repl c = c

        tuplify [a,b,c,d] = ((a,b),(c,d))

type State = [Int]
mapsize = 1000

-- Instead of keeping a map of all visited spots, we only keep their
-- coordinates. In fact, we only keep an id of their coordinates by
-- keeping x+mapsize*y. Prepending to a list is also faster than
-- appending.
visit :: (Int, Int) -> State -> State
visit (x,y) l = (x+mapsize*y):l

-- This takes two numbers and returns a list of all in-between,
-- regardless of order
get_range :: (Int, Int) -> [Int]
get_range (a, b) | a < b     = [a..b]
                 | otherwise = reverse $ [b..a]

-- This takes an instruction and marks all points covered as visited.
-- For Part A this is just straight lines, i.e. either x1=x2 or y1=y2.
evolveMapA :: Instr -> State -> State
evolveMapA ((x1,y1), (x2,y2)) state
    | y1 == y2    = foldr visit state $
                    zip t1 (replicate (length t1) y1)
    | x1 == x2    = foldr visit state $
                    zip (replicate (length t2) x1) t2
    | otherwise   = state
    where
        t1 = get_range (x1,x2)
        t2 = get_range (y1,y2)


-- Given an element a and a list [a,a,...,a,b,c,d,e,...] this eats the
-- duplicates and returns [b,c,d,e,...]. If the list doesn't start
-- with a, i.e. [b,c,d,e,...] it just returns the list back
eatDuplicates :: Int -> [Int] -> [Int]
eatDuplicates a [] = []
eatDuplicates a b | a == (head b) = eatDuplicates a $ tail b
                  | otherwise     = b
-- This returns the number of non-unique elements in a sorted (!) list
-- by keeping track of the current first element and an accumulator.
-- If the next element is the previous one, it increases the count and
-- eats all duplicates.
countNonUnique :: (Int, Int) -> [Int] -> Int
countNonUnique (prev, count) [] = count
countNonUnique (prev, count) a
    | prev == (head a) = countNonUnique (prev, count+1) $ eatDuplicates prev $ tail a
    | otherwise        = countNonUnique (head a, count) $ tail a

solveDay5A :: [String] -> Int
solveDay5A lines = countNonUnique (-1,0) $
                   sort $
                   foldr evolveMapA [] $
                   map loadLine $ lines


evolveMapB :: Instr -> State -> State
evolveMapB ((x1,y1), (x2,y2)) state
    | y1 == y2    = foldr visit state $
                    zip t1 (replicate (length t1) y1)
    | x1 == x2    = foldr visit state $
                    zip (replicate (length t2) x1) t2
    | otherwise   = foldr visit state $
                    zip t1 t2
    where
        t1 = get_range (x1,x2)
        t2 = get_range (y1,y2)


solveDay5B :: [String] -> Int
solveDay5B lines = countNonUnique (-1,0) $
                   sort $
                   foldr evolveMapB [] $
                   map loadLine $ lines

main = do
    contents <- readFile "input05.txt"
    print $ solveDay5A $ lines $ contents
    print $ solveDay5B $ lines $ contents

