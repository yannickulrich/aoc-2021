import Data.List
data U = N Int | C Char  deriving (Eq)
instance Show U where
    show (N a) = show a
    show (C a) = show a

flatten arr = [y | x<- arr, y <- x]

toint c = fromEnum c - 48
parse1 :: String -> [U]
parse1 s = map go $ filter (/= ',') $ s
    where
        go '[' = C '['
        go ']' = C ']'
        go x = N $ toint x


smap :: (c -> a -> (b, c)) -> c -> [a] -> [b]
smap _ _ [] = []
smap f s (x:xs) = fx : smap f ns xs
    where
        (fx, ns) = f s x


explodeR :: [U] -> [U]
explodeR ((N a):r) = tail $ smap go (Just a) $ r
    where
        go :: Maybe Int -> U -> (U, Maybe Int)
        go Nothing p = (p, Nothing)
        go (Just a) (N b) = (N (a+b), Nothing)
        go (Just a) (C c) = (C c, Just a)

explodeL :: [U] -> [U]
explodeL l = reverse $ explodeR $ reverse $ l

explode :: [U] -> [U]
explode p = case nSplit of
                Nothing -> p
                Just n -> (
                    (explodeL $ fst $ splitAt (n+1) $ p) ++
                    [N 0] ++
                    (explodeR $ snd $ splitAt (n+1) $ p)
                    )
    where
        nSplit = elemIndex 5 $ scanl go 0 $ p
        go acc (C '[') = acc+1
        go acc (C ']') = acc-1
        go acc (N _) = acc

test1Explode :: String -> String -> Bool
test1Explode a b = (explode $ parse1 $ a) == (parse1 $ b)

testExplode = and $ [test1Explode "[[[[[9,8],1],2],3],4]" "[[[[0,9],2],3],4]",
                     test1Explode "[7,[6,[5,[4,[3,2]]]]]" "[7,[6,[5,[7,0]]]]",
                     test1Explode "[[6,[5,[4,[3,2]]]],1]" "[[6,[5,[7,0]]],3]",
                     test1Explode "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]" "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]",
                     test1Explode "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]" "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"]

fdiv a  | odd a      = (a-1) `div` 2
        | otherwise  = a `div` 2
cdiv a  | odd a      = (a+1) `div` 2
        | otherwise  = a `div` 2

split :: [U] -> [U]
split p = flatten $ smap go True $ p
    where
        go :: Bool -> U -> ([U], Bool)
        go f (C c) = ([C c], f)
        go True  (N a) | a >= 10    = ([C '[',
                                        N $ fdiv $ a,
                                        N $ cdiv $ a,
                                        C ']'], False)
                       | otherwise  = ([N a], True)
        go False (N a) = ([N a], False)

testSplit = (split $ split $ explode $ parse1 $ "[[[[0,7],4],[7,[[8,4],9]]],[1,1]]") == (parse1 $ "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]")

fixed :: Eq a => (a -> a) -> a -> a
fixed f a  | fa == a   = a
           | otherwise = fixed f fa
    where
        fa = f a

reduce1 :: [U] -> [U]
reduce1 a = if e == a then s else e
    where
        s = split $ e
        e = explode $ a

reduce :: [U] -> [U]
reduce a = fixed reduce1 a

add :: [U] -> [U] -> [U]
add a b = reduce $ [C '['] ++ a ++ b ++ [C ']']

testAdd = (add (parse1 "[[[[4,3],4],4],[7,[[8,4],9]]]") (parse1 "[1,1]")) == (parse1 "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]")


total :: [[U]] -> [U]
total (a:as) = foldl add a $ as

magnitude :: [U] -> Int
magnitude a = fst $ go $ a
    where
        go :: [U] -> (Int, [U])
        go ((C ']'):leaves) = go leaves
        go ((C '['):leaves) = (3 * ln + 2 * rn, rest)
            where
                (rn, rest) = go rs
                (ln, rs) = go leaves
        go ((N x):xs) = (x, xs)


solveDay18A :: String -> Int
solveDay18A contents = magnitude $
                       total $
                       map parse1 $
                       lines $
                       contents

tuples :: [a] -> [(a, a)]
tuples a = [(i, j)  | i <- a,
                      j <- a]

solveDay18B :: String -> Int
solveDay18B contents = maximum $
                       map magnitude $
                       map (uncurry $ add) $
                       tuples $
                       map parse1 $
                       lines $
                       contents

main = do
    contents <- readFile "input18.txt"
    print $ solveDay18A $ contents
    print $ solveDay18B $ contents
