import Debug.Trace
import qualified Data.Set as S

type Point = (Int, Int)
type Points = S.Set Point
type Map = (Points, Points)
type Dim = (Int, Int)
type State = (Map, (Bool, Int))

indexMap :: [String] -> [Point]
indexMap dat = [ (i,j) | i <- [0..length dat-1],
                         j <- [0..length (head dat)-1]]

iMovers :: Char -> [String] -> [Point]
iMovers c dat = filter (f c) $
                indexMap $
                dat
    where
        f c (i,j) = c == dat!!i!!j

eMovers = iMovers '>'
sMovers = iMovers 'v'

buildState :: String -> State
buildState contents = ((S.fromList $ eMovers $ dat,
                        S.fromList $ sMovers $ dat),
                       (False, 0))
    where dat = lines $ contents

evolveEast :: (Int, Int) -> Map -> Points
evolveEast (w,h) (e,s) = S.map go $ e
    where
        go o@(y,x)  | S.member n e = o
                    | S.member n s = o
                    | otherwise  = n
            where n = (y, (x+1) `mod` w)

evolveSouth :: (Int, Int) -> Map -> Points
evolveSouth (w,h) (e,s) = S.map go $ s
    where
        go o@(y,x)  | S.member n e = o
                    | S.member n s = o
                    | otherwise  = n
            where n = ((y+1) `mod` h, x)

evolve :: (Int, Int) -> State -> State
evolve (w,h) ((e,s), (_, n)) = ((en, sn),
                                ((sn == s) && (en == e), n+1))
    where
        sn = evolveSouth(w,h) $ (en,s)
        en = evolveEast (w,h) $ (e ,s)

check :: State -> Bool
check (_, (a, _)) = a

solveDay25A :: String -> Int
solveDay25A contents = snd $ snd $
                       until check (evolve dims) $
                       buildState $
                       contents
    where dims = (length $ head $ lines $ contents,
                  length $        lines $ contents)

main = do
    contents <- readFile "input25.txt"
    print $ solveDay25A $ contents
