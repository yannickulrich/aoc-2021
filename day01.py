with open('input01.txt') as fp:
    txt = [int(i) for i in fp.read().splitlines()]

c = 0
for i in range(len(txt)-1):
    if txt[i + 1] > txt[i]:
        c += 1
print(c)


## part 2
c = 0
winodws = [sum(txt[i:i+3]) for i in range(len(txt)-2)]
for i in range(len(winodws)-1):
    if winodws[i + 1] > winodws[i]:
        c += 1
print(c)
