import Data.List

close :: Char -> Char
close '(' = ')'
close '[' = ']'
close '{' = '}'
close '<' = '>'

bsAdd :: Char -> ([Char],Char) -> ([Char],Char)
bsAdd c (state,co) | co `elem` ")}]>"          = ([], co)
                   | c  `elem` "({[<"          = (c:state,co)
                   | (c == (close$head$state)) = (tail $ state,co)
                   | otherwise                 = ([], c)

buildscope :: [Char] -> ([Char], Char)
buildscope line = foldr bsAdd ([],' ') $ reverse $ line

scoreOneA :: ([Char], Char) -> Integer
scoreOneA ([], ')') = 3
scoreOneA ([], ']') = 57
scoreOneA ([], '}') = 1197
scoreOneA ([], '>') = 25137
scoreOneA _ = 0

solveDay10A :: [String] -> Integer
solveDay10A lines = sum $
                    map scoreOneA $
                    map buildscope $
                    lines

scoreOneB :: Char -> Integer
scoreOneB ')' = 1
scoreOneB ']' = 2
scoreOneB '}' = 3
scoreOneB '>' = 4

scoreBdo :: Char -> Integer -> Integer
scoreBdo c state = 5*state + scoreOneB c

scoreB :: [Char] -> Integer
scoreB inp = foldr scoreBdo 0 $ reverse $ inp

median :: [Integer] -> Integer
median lst = lst !! ( (length lst) `div` 2)

solveDay10B :: [String] -> Integer
solveDay10B lines = median $
                    sort $
                    map scoreB $
                    map (map close) $
                    map fst $
                    filter (\(x,y) -> y == ' ') $
                    map buildscope $
                    lines

main = do

    contents <- readFile "input10.txt"
    print $ solveDay10A $ lines $ contents
    print $ solveDay10B $ lines $ contents
