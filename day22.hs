type Cube = ((Integer, Integer), (Integer, Integer), (Integer, Integer))
type Instr = (Bool, Cube)

toint :: String -> Integer
toint a = read a :: Integer

chopInt :: (Integer, Integer, Integer, Integer) -> [(Integer, Integer, Bool)]
chopInt (iA0, iA1, iB0, iB1)
    | iA0 == iA1         = []
    | iB0 == iB1         = [(iA0, iA1, False)]
    | (iB0 <  iB1) && (iB1 <= iA0) && (iA0 <  iA1)  = [(iA0, iA1, False)]
    | (iB0 <  iA0) && (iA0 <  iB1) && (iB1 <  iA1)  = [(iA0, iB1, True), (iB1, iA1, False)]
    | (iA0 <  iB0) && (iB0 <  iB1) && (iB1 <  iA1)  = [(iA0, iB0,False), (iB0, iB1,True), (iB1, iA1,False)]
    | (iA0 <  iB0) && (iB0 <  iA1) && (iA1 <  iB1)  = [(iA0, iB0,False), (iB0, iA1,True)]
    | (iA0 <  iA1) && (iA1 <  iB0) && (iB0 <  iB1)  = [(iA0, iA1, False)]
    | (iB0 <= iA0) && (iA0 <  iA1) && (iA1 <= iB1)  = [(iA0, iA1, True)]
    | (iA0 <  iA1) && (iA1 == iB0) && (iB0 <  iB1)  = [(iA0, iA1, False)]
    | (iA0 == iB0) && (iB0 <  iB1) && (iB1 <  iA1)  = [(iA0, iB1, True), (iB1, iA1, False)]
    | (iA0 <  iB0) && (iB0 <  iA1) && (iA1 == iB1)  = [(iA0, iB0, False), (iB0, iA1, True)]

flatten arr = [y | x<- arr, y <- x]

splitCube :: Cube -> Cube -> [Cube]
splitCube cB@((xB0,xB1), (yB0,yB1), (zB0,zB1))
          cA@((xA0,xA1), (yA0,yA1), (zA0,zA1))
    | (xA0 > xB1) || (xA1 < xB0)  = [cA]
    | (yA0 > yB1) || (yA1 < yB0)  = [cA]
    | (zA0 > zB1) || (zA1 < zB0)  = [cA]
    | otherwise  = flatten $ map goX $ chopX

    where
        goX (x0,x1,False) = [((x0,x1),(yA0,yA1),(zA0,zA1))]
        goX (x0,x1,True)  = flatten $ map goY $ chopY
          where
            goY (y0,y1,False) = [((x0,x1),(y0,y1),(zA0,zA1))]
            goY (y0,y1,True)  = flatten $ map goZ $ chopZ
              where
                goZ (z0,z1,False) = [((x0,x1),(y0,y1),(z0,z1))]
                goZ (z0,z1,True)  = []

        chopX = chopInt (xA0, xA1, xB0, xB1)
        chopY = chopInt (yA0, yA1, yB0, yB1)
        chopZ = chopInt (zA0, zA1, zB0, zB1)

build1 :: Instr -> [Cube] -> [Cube]
build1 (True, c) a = c:a
build1 (False, c) a = flatten $ map (splitCube c) $ a

build :: [Instr] -> [Cube]
build instr = foldr build1 [] $ reverse $ instr

v1 :: Cube -> Integer
v1 ((x0,x1), (y0,y1), (z0,z1)) = (x1-x0) * (y1-y0) * (z1-z0)

volume1 :: ([Cube], Integer) -> ([Cube], Integer)
volume1 ([], v) = ([], v)
volume1 ((c:on), v) = volume1 (onon, v+vv)
    where
        vv = v1 c
        onon = flatten $ map (splitCube c) $ on

volume :: [Cube] -> Integer
volume on = snd $ volume1 (on, 0)

play :: [Instr] -> Integer
play = volume.build

parseLine :: String -> Instr
parseLine line = ("on" == l!!0,(
                   (toint $ l!!2,(toint $ l!!3) + 1),
                   (toint $ l!!5,(toint $ l!!6) + 1),
                   (toint $ l!!8,(toint $ l!!9) + 1)))
    where
        l = words $ line

parse :: String -> [Instr]
parse contents = map parseLine $
                 lines $
                 map repl $
                 contents
    where
        repl '=' = ' '
        repl '.' = ' '
        repl ',' = ' '
        repl a = a

filterA :: Instr -> Bool
filterA (_,((x0,x1),(y0,y1),(z0,z1))) = (-50 <= x0) && (x1 < 50) &&
                                        (-50 <= y0) && (y1 < 50) &&
                                        (-50 <= z0) && (z1 < 50)

solveDay22A :: String -> Integer
solveDay22A contents = play $
                       filter filterA $
                       parse $
                       contents
solveDay22B :: String -> Integer
solveDay22B contents = play $
                       parse $
                       contents

main = do
    contents <- readFile "input22.txt"
    print $ solveDay22A $ contents
    print $ solveDay22B $ contents
