import re

txt = """NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C"""
txt = open('input14.txt').read()

templ=txt.splitlines()[0]
insrt = {(a,b):c for a,b,c in re.findall('(.)(.) -> (.)', txt)}


def step(ans):
    ansNew = []
    for i in range(len(ans)-1):
        ansNew.append(ans[i])
        new = insrt[(ans[i],ans[i+1])]
        ansNew.append(new)
    ansNew.append(ans[-1])
    return ansNew

ans = [i for i in templ]
for i in range(10):
    ans = step(ans)

lets = list(set(ans))
occ = [ans.count(i) for i in lets]

print(max(occ)-min(occ))


def str2couples(templ, insrt):
    all_couples = list(zip(templ[:-1], templ[1:]))

    lets = sorted(list(set(templ)) + list(set(insrt.values())))
    couples = [(a,b) for a in lets for b in lets]
    return {
        i: all_couples.count(i)
        for i in couples
    }

def step2(c):
    ans = {i: v for i,v in c.items()}

    for (a,b), v in c.items():
        n = insrt[(a,b)]
        old = c[(a,b)]
        #ab -> an nb
        ans[(a,n)] += old
        ans[(n,b)] += old
        ans[(a,b)] -= old
    return ans

ans2 = str2couples(templ, insrt)
for i in range(40):
    ans2 = step2(ans2)

occ2 = {i: 0 for i in lets}
occ2[templ[+0]] = 1
occ2[templ[-1]] = 1
for (a,b), v in ans2.items():
    occ2[a] += v
    occ2[b] += v
occ2 = [occ2[i]//2 for i in lets]
print(max(occ2)-min(occ2))
